### Compilation in Docker

```
docker run -it ubuntu:rolling /bin/bash

export SONAR_SCANNER_OPTS="-Xmx4096m"

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update
apt install -y git
git clone https://github.com/torvalds/linux.git

cd linux

apt build-dep -y linux

make allnoconfig

make -j8
```

### Gitlab Docker 

```
####################
    - export SONAR_SCANNER_OPTS="-Xmx4096m"

    - sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

    - export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

    - apt update

    - apt build-dep -y linux

    - make allnoconfig
####################

----- make -j8

```
